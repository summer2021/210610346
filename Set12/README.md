# Guideline to Transfer Set12 to MindRecord

<!-- TOC -->

- [What does the example do](#what-does-the-example-do)
- [How to use the example to generate MindRecord](#how-to-use-the-example-to-generate-mindrecord)
    - [Download Set12 dataset](#download-Set12-dataset)
    - [Generate MindRecord](#generate-mindrecord)
    - [Create MindDataset By MindRecord](#create-minddataset-by-mindrecord)

<!-- /TOC -->

## What does the example do

This example is used to read data from Set12 and generate mindrecord. It just transfers the Set12 to mindrecord without any data preprocessing. You can modify the example or follow the example to implement your own example.

1. run.sh: generate MindRecord entry script.
    - Set12_to_mindrecord.py : read the Set12 and transfer it to mindrecord.
2. run_create.sh: create MindDataset by MindRecord entry script.
    - Set12_create.py: use MindDataset to read MindRecord to generate dataset.

## How to use the example to generate MindRecord

Download Set12 dataset, transfer it to mindrecord, use MindDataset to read mindrecord.

### Download Set12 dataset

1. Download the data.  

2. Unzip the data to dir example/cv_to_mindrecord/Set12/dataset.

 The unzip should like this:

```bash
$ ls {your-mindspore}/example/cv_to_mindrecord/Set12/dataset/
set12
```

### Generate MindRecord

1.Run the run.sh script.

```bash
bash run.sh
```
2.Output like this:

```bash
>> begin generate mindrecord
>> transformed 12 train record...
>> generated successfully

```
3.Generate mindrecord files

```bash
$ ls output/
train.mindrecord  train.mindrecord.db 

```

### Create MindDataset By MindRecord

1. Run the run_create.sh script.

```bash
bash run_create.sh
```
2. Output like this:

```bash
example 0: {'image': array([137,  80,  78, ...,  66,  96, 130], dtype=uint8), 'image_name': array(b'0010.png', dtype='|S8')}
example 1: {'image': array([137,  80,  78, ...,  66,  96, 130], dtype=uint8), 'image_name': array(b'0008.png', dtype='|S8')}
example 2: {'image': array([137,  80,  78, ...,  66,  96, 130], dtype=uint8), 'image_name': array(b'0006.png', dtype='|S8')}
example 3: {'image': array([137,  80,  78, ...,  66,  96, 130], dtype=uint8), 'image_name': array(b'0007.png', dtype='|S8')}
example 4: {'image': array([137,  80,  78, ...,  66,  96, 130], dtype=uint8), 'image_name': array(b'0003.png', dtype='|S8')}
example 5: {'image': array([137,  80,  78, ...,  66,  96, 130], dtype=uint8), 'image_name': array(b'0001.png', dtype='|S8')}
example 6: {'image': array([137,  80,  78, ...,  66,  96, 130], dtype=uint8), 'image_name': array(b'0004.png', dtype='|S8')}
example 7: {'image': array([137,  80,  78, ...,  66,  96, 130], dtype=uint8), 'image_name': array(b'0002.png', dtype='|S8')}
example 8: {'image': array([137,  80,  78, ...,  66,  96, 130], dtype=uint8), 'image_name': array(b'0009.png', dtype='|S8')}
example 9: {'image': array([137,  80,  78, ...,  66,  96, 130], dtype=uint8), 'image_name': array(b'0000.png', dtype='|S8')}
example 10: {'image': array([137,  80,  78, ...,  66,  96, 130], dtype=uint8), 'image_name': array(b'0011.png', dtype='|S8')}
example 11: {'image': array([137,  80,  78, ...,  66,  96, 130], dtype=uint8), 'image_name': array(b'0005.png', dtype='|S8')}
>> total rows: 12
create successfully

```


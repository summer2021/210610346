"""write Set12 data to mindrecord file"""
import os
import numpy as np
from mindspore.mindrecord import FileWriter


Set12_TRAIN_PATH = "dataset/set12"

MINDRECORD_TRAIN_FILE_NAME = "output_Set12/train.mindrecord"

#get file name
train_image_name=[]

"""get train data from dataset"""
for file in os.listdir(Set12_TRAIN_PATH):     
    if file.endswith(".png"):
        train_image_name.append(file)

# train
def get_data_as_dict_train():
    """generate mindrecord"""
    for line in train_image_name:
        data={}
        image_path=os.path.join(os.path.join(Set12_TRAIN_PATH, line))
        image_content=open(image_path, "rb")
        image_bytes = image_content.read()
        image_content.close()
        data["image"] = image_bytes 
        data["image_name"] = line         
        yield data
  

def gen_mindrecord():
    """gen mindreocrd"""
    fw_train = FileWriter(MINDRECORD_TRAIN_FILE_NAME)
    schema = {"image": {"type": "bytes"},
              "image_name": {"type": "string"}
             }
    fw_train.add_schema(schema, "Set12 dataset")

    get_data_iter_train= get_data_as_dict_train()
    
    batch_size = 12
    transform_count_train = 0

    data_list_train = []
    
    for _ in range(batch_size):
        data_list_train.append(get_data_iter_train.__next__())
        transform_count_train += 1
    fw_train.write_raw_data(data_list_train)
    print(">> transformed {} train record...".format(transform_count_train))

    fw_train.commit()
    print(">>generated successfully")


def main():
    """generate mindrecord"""
    print(">> begin generate mindrecord")
    gen_mindrecord()

if __name__ == "__main__":
    main()

"""write pbn data to mindrecord file"""
import os
import numpy as np
from mindspore.mindrecord import FileWriter


PBN_TRAIN_PATH = "dataset/train"
PBN_TEST_PATH = "dataset/test"

MINDRECORD_TRAIN_FILE_NAME = "output_pbn/train.mindrecord"
MINDRECORD_TEST_FILE_NAME="output_pbn/test.mindrecord"

#get file name
train_image_name=[]
test_image_name=[]

"""get train and test data from dataset"""
for file in os.listdir(PBN_TRAIN_PATH):     
    if file.endswith(".jpg"):
        train_image_name.append(file)
for file in os.listdir(PBN_TEST_PATH):     
    if file.endswith(".jpg"):
        test_image_name.append(file)

# train
def get_data_as_dict_train():
    """get and yield data"""
    for line in train_image_name:
        data={}
        image_path=os.path.join(os.path.join(PBN_TRAIN_PATH, line))
        image_content=open(image_path, "rb")
        image_bytes = image_content.read()
        image_content.close()
        data["image"] = image_bytes 
        data["image_name"] = line         
        yield data
# test

def get_data_as_dict_test():  
    """gen mindreocrd"""
    for line in test_image_name:
        data={}
        image_path=os.path.join(os.path.join(PBN_TEST_PATH, line))
        image_content=open(image_path, "rb")
        image_bytes = image_content.read()
        image_content.close()
        data["image"] = image_bytes 
        data["image_name"] = line         
        yield data
t=get_data_as_dict_test()
    
def gen_mindrecord():
    # train test to mindrecord
    fw_train = FileWriter(MINDRECORD_TRAIN_FILE_NAME)
    fw_test =FileWriter(MINDRECORD_TEST_FILE_NAME)
    schema = {"image": {"type": "bytes"},
              "image_name": {"type": "string"}
             }
    fw_train.add_schema(schema, "pbn dataset")
    fw_test.add_schema(schema, "pbn dataset")

    get_data_iter_train= get_data_as_dict_train()
    get_data_iter_test=get_data_as_dict_test()
    
    batch_size = 256
    transform_count_train = 0
    transform_count_test = 0
    
    while True:
        data_list_train = []
        data_list_test=[]
        try:
            for _ in range(batch_size):
                data_list_train.append(get_data_iter_train.__next__())
                data_list_test.append(get_data_iter_test.__next__())
                transform_count_test += 1
                transform_count_train += 1
            fw_train.write_raw_data(data_list_train)
            fw_test.write_raw_data(data_list_test)
            print(">> transformed {} train record...".format(transform_count_train))
            print(">> transformed {} test record...".format(transform_count_test))
        except StopIteration:
            if data_list_train:
                fw_train.write_raw_data(data_list_train)
            if data_list_test:
                fw_test.write_raw_data(data_list_test)
            print(">> transformed {} train record...".format(transform_count_train))
            print(">> transformed {} test record...".format(transform_count_test))
            break

    fw_train.commit()
    fw_test.commit()
    print(">>generated successfully")


def main():
    """run gen_mindrecord"""
    print(">> begin generate mindrecord")
    gen_mindrecord()

if __name__ == "__main__":
    main()

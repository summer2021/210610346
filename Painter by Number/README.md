# Guideline to Painter by Number Dataset to MindRecord

<!-- TOC -->

- [What does the example do](#what-does-the-example-do)
- [How to use the example to generate MindRecord](#how-to-use-the-example-to-generate-mindrecord)
    - [Download Download Painter by Number and unzip and unzip](#download-painter-by-number-and-unzip)
    - [Generate MindRecord](#generate-mindrecord)
    - [Create MindDataset By MindRecord](#create-minddataset-by-mindrecord)

<!-- /TOC -->

## What does the example do

This example is used to read data from Painter by Number dataset and generate mindrecord. It just transfers the Painter by Number to mindrecord without any data preprocessing. You can modify the example or follow the example to implement your own example.

1. run.sh: generate MindRecord entry script.
    - pbn_to_mindrecord.py : read the Painter by Number and transfer it to mindrecord.
2. run_create.py: create MindDataset by MindRecord entry script.
    - pbn_create.py: use MindDataset to read MindRecord to generate dataset.

## How to use the example to generate MindRecord

DownloadPainter by Number, transfer it to mindrecord, use MindDataset to read mindrecord.

### Download Painter by Number Dataset and unzip

1. Download the training data zip.
> [Painter by Number download address](https://github.com/magenta/magenta/tree/master/magenta/models/arbitrary_image_stylization)    

2. Unzip the data to dir example/cv_to_mindrecord/Painter by Number/dataset.

 The unzip should like this:

```bash
$ ls {your-mindspore}/example/cv_to_mindrecord/Painter by Number/dataset/
train test
```

### Generate MindRecord

1.Run the run.sh script.

```bash
bash run.sh
```

2.Output like this:

```bash
>> begin generate mindrecord
>> transformed 256 train record...
>> transformed 256 test record...
>> transformed 512 train record...
...
>> transformed 23552 test record...
>> transformed 23808 train record...
>> transformed 23808 test record...
>> transformed 23817 train record...
>> transformed 23817 test record...
>>generated successfully

```

3.Generate mindrecord files

```bash
$ ls output/
train.mindrecord  test.mindrecord.db 
```

### Create MindDataset By MindRecord

1. Run the run_create.sh script.

```bash
bash run_create.sh
 ```

2. Output like this:

```bash
  example 0: {'image': array([255, 216, 255, ..., 207, 255, 217], dtype=uint8), 'image_name': array(b'25280.jpg', dtype='|S9')}
example 1: {'image': array([255, 216, 255, ..., 199, 255, 217], dtype=uint8), 'image_name': array(b'102274.jpg', dtype='|S10')}
example 2: {'image': array([255, 216, 255, ..., 127, 255, 217], dtype=uint8), 'image_name': array(b'23126.jpg', dtype='|S9')}
example 3: {'image': array([255, 216, 255, ..., 143, 255, 217], dtype=uint8), 'image_name': array(b'26376.jpg', dtype='|S9')}
example 4: {'image': array([255, 216, 255, ...,  31, 255, 217], dtype=uint8), 'image_name': array(b'10107.jpg', dtype='|S9')}
example 5: {'image': array([255, 216, 255, ..., 220, 255, 217], dtype=uint8), 'image_name': array(b'30300.jpg', dtype='|S9')}
example 6: {'image': array([255, 216, 255, ...,  31, 255, 217], dtype=uint8), 'image_name': array(b'2707.jpg', dtype='|S8')}
example 0: {'image': array([255, 216, 255, ..., 207, 255, 217], dtype=uint8), 'image_name': array(b'25280.jpg', dtype='|S9')}
example 1: {'image': array([255, 216, 255, ..., 199, 255, 217], dtype=uint8), 'image_name': array(b'102274.jpg', dtype='|S10')}
example 2: {'image': array([255, 216, 255, ..., 127, 255, 217], dtype=uint8), 'image_name': array(b'23126.jpg', dtype='|S9')}
example 3: {'image': array([255, 216, 255, ..., 143, 255, 217], dtype=uint8), 'image_name': array(b'26376.jpg', dtype='|S9')}
example 4: {'image': array([255, 216, 255, ...,  31, 255, 217], dtype=uint8), 'image_name': array(b'10107.jpg', dtype='|S9')}
example 5: {'image': array([255, 216, 255, ..., 220, 255, 217], dtype=uint8), 'image_name': array(b'30300.jpg', dtype='|S9')}
example 6: {'image': array([255, 216, 255, ...,  31, 255, 217], dtype=uint8), 'image_name': array(b'2707.jpg', dtype='|S8')}
...
example 23806: {'image': array([255, 216, 255, ..., 153, 255, 217], dtype=uint8), 'image_name': array(b'87545.jpg', dtype='|S9')}
example 23807: {'image': array([255, 216, 255, ...,  63, 255, 217], dtype=uint8), 'image_name': array(b'41601.jpg', dtype='|S9')}
example 23808: {'image': array([255, 216, 255, ..., 207, 255, 217], dtype=uint8), 'image_name': array(b'76797.jpg', dtype='|S9')}
example 23809: {'image': array([255, 216, 255, ..., 132, 255, 217], dtype=uint8), 'image_name': array(b'91166.jpg', dtype='|S9')}
example 23810: {'image': array([255, 216, 255, ..., 161, 255, 217], dtype=uint8), 'image_name': array(b'90986.jpg', dtype='|S9')}
example 23811: {'image': array([255, 216, 255, ...,   0, 255, 217], dtype=uint8), 'image_name': array(b'87930.jpg', dtype='|S9')}
example 23812: {'image': array([255, 216, 255, ...,   3, 255, 217], dtype=uint8), 'image_name': array(b'11607.jpg', dtype='|S9')}
example 23813: {'image': array([255, 216, 255, ..., 220, 255, 217], dtype=uint8), 'image_name': array(b'31897.jpg', dtype='|S9')}
example 23814: {'image': array([255, 216, 255, ...,  49, 255, 217], dtype=uint8), 'image_name': array(b'81522.jpg', dtype='|S9')}
example 23815: {'image': array([255, 216, 255, ...,  72, 255, 217], dtype=uint8), 'image_name': array(b'9547.jpg', dtype='|S8')}
example 23816: {'image': array([255, 216, 255, ..., 137, 255, 217], dtype=uint8), 'image_name': array(b'81250.jpg', dtype='|S9')}
>> total rows: 23817
>>create successfully
```

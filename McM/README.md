# Guideline to Transfer McM to MindRecord

<!-- TOC -->

- [What does the example do](#what-does-the-example-do)
- [How to use the example to generate MindRecord](#how-to-use-the-example-to-generate-mindrecord)
    - [Download McM dataset](#download-McM-dataset)
    - [Generate MindRecord](#generate-mindrecord)
    - [Create MindDataset By MindRecord](#create-minddataset-by-mindrecord)

<!-- /TOC -->

## What does the example do

This example is used to read data from McM and generate mindrecord. It just transfers the McM to mindrecord without any data preprocessing. You can modify the example or follow the example to implement your own example.

1. run.sh: generate MindRecord entry script.
    - McM_to_mindrecord.py : read the McM and transfer it to mindrecord.
2. run_create.sh: create MindDataset by MindRecord entry script.
    - McM_create.py: use MindDataset to read MindRecord to generate dataset.

## How to use the example to generate MindRecord

Download McM dataset, transfer it to mindrecord, use MindDataset to read mindrecord.

### Download McM dataset

1. Download the data.  

2. Unzip the data to dir example/cv_to_mindrecord/McM/dataset.

 The unzip should like this:

```bash
$ ls {your-mindspore}/example/cv_to_mindrecord/McM/dataset/
mcm
```

### Generate MindRecord

1.Run the run.sh script.

```bash
bash run.sh
```
2.Output like this:

```bash
>> begin generate mindrecord
>> transformed 18 train record...
>>generated successfully

```
3.Generate mindrecord files

```bash
$ ls output_McM/
train.mindrecord  train.mindrecord.db 

```

### Create MindDataset By MindRecord

1. Run the run_create.sh script.

```bash
bash run_create.sh
```
2. Output like this:

```bash
example 0: {'image': array([73, 73, 42, ...,  0,  0,  1], dtype=uint8), 'image_name': array(b'7.tif', dtype='|S5')}
example 1: {'image': array([73, 73, 42, ...,  0,  0,  1], dtype=uint8), 'image_name': array(b'2.tif', dtype='|S5')}
example 2: {'image': array([73, 73, 42, ...,  0,  0,  1], dtype=uint8), 'image_name': array(b'8.tif', dtype='|S5')}
example 3: {'image': array([73, 73, 42, ...,  0,  0,  1], dtype=uint8), 'image_name': array(b'14.tif', dtype='|S6')}
example 4: {'image': array([73, 73, 42, ...,  0,  0,  1], dtype=uint8), 'image_name': array(b'3.tif', dtype='|S5')}
example 5: {'image': array([73, 73, 42, ...,  0,  0,  1], dtype=uint8), 'image_name': array(b'12.tif', dtype='|S6')}
example 6: {'image': array([73, 73, 42, ...,  0,  0,  1], dtype=uint8), 'image_name': array(b'16.tif', dtype='|S6')}
example 7: {'image': array([73, 73, 42, ...,  0,  0,  1], dtype=uint8), 'image_name': array(b'9.tif', dtype='|S5')}
example 8: {'image': array([73, 73, 42, ...,  0,  0,  1], dtype=uint8), 'image_name': array(b'10.tif', dtype='|S6')}
example 9: {'image': array([73, 73, 42, ...,  0,  0,  1], dtype=uint8), 'image_name': array(b'11.tif', dtype='|S6')}
example 10: {'image': array([73, 73, 42, ...,  0,  0,  1], dtype=uint8), 'image_name': array(b'17.tif', dtype='|S6')}
example 11: {'image': array([73, 73, 42, ...,  0,  0,  1], dtype=uint8), 'image_name': array(b'18.tif', dtype='|S6')}
example 12: {'image': array([73, 73, 42, ...,  0,  0,  1], dtype=uint8), 'image_name': array(b'1.tif', dtype='|S5')}
example 13: {'image': array([73, 73, 42, ...,  0,  0,  1], dtype=uint8), 'image_name': array(b'5.tif', dtype='|S5')}
example 14: {'image': array([73, 73, 42, ...,  0,  0,  1], dtype=uint8), 'image_name': array(b'6.tif', dtype='|S5')}
example 15: {'image': array([73, 73, 42, ...,  0,  0,  1], dtype=uint8), 'image_name': array(b'13.tif', dtype='|S6')}
example 16: {'image': array([73, 73, 42, ...,  0,  0,  1], dtype=uint8), 'image_name': array(b'15.tif', dtype='|S6')}
example 17: {'image': array([73, 73, 42, ...,  0,  0,  1], dtype=uint8), 'image_name': array(b'4.tif', dtype='|S5')}
>> total rows: 18
create successfully


```

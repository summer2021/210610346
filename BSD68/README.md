# Guideline to Transfer BSD68 to MindRecord

<!-- TOC -->

- [What does the example do](#what-does-the-example-do)
- [How to use the example to generate MindRecord](#how-to-use-the-example-to-generate-mindrecord)
    - [Download BSD68 dataset](#download-BSD68-dataset)
    - [Generate MindRecord](#generate-mindrecord)
    - [Create MindDataset By MindRecord](#create-minddataset-by-mindrecord)

<!-- /TOC -->

## What does the example do

This example is used to read data from BSD68 and generate mindrecord. It just transfers the BSD68 to mindrecord without any data preprocessing. You can modify the example or follow the example to implement your own example.

1. run.sh: generate MindRecord entry script.
    - BSD68_to_mindrecord.py : read the BSD68 and transfer it to mindrecord.
2. run_create.sh: create MindDataset by MindRecord entry script.
    - BSD68_create.py: use MindDataset to read MindRecord to generate dataset.

## How to use the example to generate MindRecord

Download BSD68 dataset, transfer it to mindrecord, use MindDataset to read mindrecord.

### Download BSD68 dataset

1. Download the data.  

2. Unzip the data to dir example/cv_to_mindrecord/BSD68/dataset.

 The unzip should like this:

```bash
$ ls {your-mindspore}/example/cv_to_mindrecord/BSD68/dataset/
bsd68
```

### Generate MindRecord

1.Run the run.sh script.

```bash
bash run.sh
```
2.Output like this:

```bash
>> begin generate mindrecord
>> transformed 68 train record...
>>generated successfully

```
3.Generate mindrecord files

```bash
$ ls output_BSD68/
train.mindrecord  train.mindrecord.db 

```

### Create MindDataset By MindRecord

1. Run the run_create.sh script.

```bash
bash run_create.sh
```
2. Output like this:

```bash
example 0: {'image': array([137,  80,  78, ...,  66,  96, 130], dtype=uint8), 'image_name': array(b'0007.png', dtype='|S8')}
example 1: {'image': array([137,  80,  78, ...,  66,  96, 130], dtype=uint8), 'image_name': array(b'0030.png', dtype='|S8')}
example 2: {'image': array([137,  80,  78, ...,  66,  96, 130], dtype=uint8), 'image_name': array(b'0040.png', dtype='|S8')}
example 3: {'image': array([137,  80,  78, ...,  66,  96, 130], dtype=uint8), 'image_name': array(b'0008.png', dtype='|S8')}
example 4: {'image': array([137,  80,  78, ...,  66,  96, 130], dtype=uint8), 'image_name': array(b'0034.png', dtype='|S8')}
example 5: {'image': array([137,  80,  78, ...,  66,  96, 130], dtype=uint8), 'image_name': array(b'0002.png', dtype='|S8')}
example 6: {'image': array([137,  80,  78, ...,  66,  96, 130], dtype=uint8), 'image_name': array(b'0049.png', dtype='|S8')}
example 7: {'image': array([137,  80,  78, ...,  66,  96, 130], dtype=uint8), 'image_name': array(b'0032.png', dtype='|S8')}
...
example 63: {'image': array([137,  80,  78, ...,  66,  96, 130], dtype=uint8), 'image_name': array(b'0021.png', dtype='|S8')}
example 64: {'image': array([137,  80,  78, ...,  66,  96, 130], dtype=uint8), 'image_name': array(b'0016.png', dtype='|S8')}
example 65: {'image': array([137,  80,  78, ...,  66,  96, 130], dtype=uint8), 'image_name': array(b'0064.png', dtype='|S8')}
example 66: {'image': array([137,  80,  78, ...,  66,  96, 130], dtype=uint8), 'image_name': array(b'0065.png', dtype='|S8')}
example 67: {'image': array([137,  80,  78, ...,  66,  96, 130], dtype=uint8), 'image_name': array(b'0017.png', dtype='|S8')}
>> total rows: 68
create successfully

```


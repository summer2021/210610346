"""create Kodak24 data"""
import mindspore.dataset as ds

def create_dataset(data_file):
    """create MindDataset"""
    num_readers = 4
    data_set = ds.MindDataset(dataset_file=data_file,
                              num_parallel_workers=num_readers,
                              shuffle=True)
    index = 0
    for item in data_set.create_dict_iterator(output_numpy=True, num_epochs=1):
        print("example {}: {}".format(index, item))
        index += 1
    print(">> total rows: {}".format(index))

if __name__ == '__main__':
    create_dataset('output_Kodak24/train.mindrecord')
    print("create successfully")
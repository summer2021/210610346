# Guideline to Transfer Kodak24 to MindRecord

<!-- TOC -->

- [What does the example do](#what-does-the-example-do)
- [How to use the example to generate MindRecord](#how-to-use-the-example-to-generate-mindrecord)
    - [Download Kodak24 dataset](#download-Kodak24-dataset)
    - [Generate MindRecord](#generate-mindrecord)
    - [Create MindDataset By MindRecord](#create-minddataset-by-mindrecord)

<!-- /TOC -->

## What does the example do

This example is used to read data from Kodak24 and generate mindrecord. It just transfers the Kodak24 to mindrecord without any data preprocessing. You can modify the example or follow the example to implement your own example.

1. run.sh: generate MindRecord entry script.
    - Kodak24_to_mindrecord.py : read the Kodak24 and transfer it to mindrecord.
2. run_create.sh: create MindDataset by MindRecord entry script.
    - Kodak24_create.py: use MindDataset to read MindRecord to generate dataset.

## How to use the example to generate MindRecord

Download Kodak24 dataset, transfer it to mindrecord, use MindDataset to read mindrecord.

### Download Kodak24 dataset

1. Download the data.  

2. Unzip the data to dir example/cv_to_mindrecord/Kodak24/dataset.

 The unzip should like this:

```bash
$ ls {your-mindspore}/example/cv_to_mindrecord/Kodak24/dataset/
kodak24
```

### Generate MindRecord

1.Run the run.sh script.

```bash
bash run.sh
```
2.Output like this:

```bash
>> begin generate mindrecord
>> transformed 24 train record...
>>generated successfully

```
3.Generate mindrecord files

```bash
$ ls output_Kodak24/
train.mindrecord  train.mindrecord.db 

```

### Create MindDataset By MindRecord

1. Run the run_create.sh script.

```bash
bash run_create.sh
```
2. Output like this:

```bash
example 0: {'image': array([137,  80,  78, ...,  66,  96, 130], dtype=uint8), 'image_name': array(b'kodim21.png', dtype='|S11')}
example 1: {'image': array([137,  80,  78, ...,  66,  96, 130], dtype=uint8), 'image_name': array(b'kodim15.png', dtype='|S11')}
example 2: {'image': array([137,  80,  78, ...,  66,  96, 130], dtype=uint8), 'image_name': array(b'kodim07.png', dtype='|S11')}
example 3: {'image': array([137,  80,  78, ...,  66,  96, 130], dtype=uint8), 'image_name': array(b'kodim13.png', dtype='|S11')}
example 4: {'image': array([137,  80,  78, ...,  66,  96, 130], dtype=uint8), 'image_name': array(b'kodim11.png', dtype='|S11')}
example 5: {'image': array([137,  80,  78, ...,  66,  96, 130], dtype=uint8), 'image_name': array(b'kodim17.png', dtype='|S11')}
example 6: {'image': array([137,  80,  78, ...,  66,  96, 130], dtype=uint8), 'image_name': array(b'kodim20.png', dtype='|S11')}
example 7: {'image': array([137,  80,  78, ...,  66,  96, 130], dtype=uint8), 'image_name': array(b'kodim12.png', dtype='|S11')}
example 8: {'image': array([137,  80,  78, ...,  66,  96, 130], dtype=uint8), 'image_name': array(b'kodim08.png', dtype='|S11')}
example 9: {'image': array([137,  80,  78, ...,  66,  96, 130], dtype=uint8), 'image_name': array(b'kodim14.png', dtype='|S11')}
example 10: {'image': array([137,  80,  78, ...,  66,  96, 130], dtype=uint8), 'image_name': array(b'kodim24.png', dtype='|S11')}
example 11: {'image': array([137,  80,  78, ...,  66,  96, 130], dtype=uint8), 'image_name': array(b'kodim03.png', dtype='|S11')}
example 12: {'image': array([137,  80,  78, ...,  66,  96, 130], dtype=uint8), 'image_name': array(b'kodim09.png', dtype='|S11')}
example 13: {'image': array([137,  80,  78, ...,  66,  96, 130], dtype=uint8), 'image_name': array(b'kodim22.png', dtype='|S11')}
example 14: {'image': array([137,  80,  78, ...,  66,  96, 130], dtype=uint8), 'image_name': array(b'kodim04.png', dtype='|S11')}
example 15: {'image': array([137,  80,  78, ...,  66,  96, 130], dtype=uint8), 'image_name': array(b'kodim02.png', dtype='|S11')}
example 16: {'image': array([137,  80,  78, ...,  66,  96, 130], dtype=uint8), 'image_name': array(b'kodim10.png', dtype='|S11')}
example 17: {'image': array([137,  80,  78, ...,  66,  96, 130], dtype=uint8), 'image_name': array(b'kodim16.png', dtype='|S11')}
example 18: {'image': array([137,  80,  78, ...,  66,  96, 130], dtype=uint8), 'image_name': array(b'kodim06.png', dtype='|S11')}
example 19: {'image': array([137,  80,  78, ...,  66,  96, 130], dtype=uint8), 'image_name': array(b'kodim23.png', dtype='|S11')}
example 20: {'image': array([137,  80,  78, ...,  66,  96, 130], dtype=uint8), 'image_name': array(b'kodim01.png', dtype='|S11')}
example 21: {'image': array([137,  80,  78, ...,  66,  96, 130], dtype=uint8), 'image_name': array(b'kodim05.png', dtype='|S11')}
example 22: {'image': array([137,  80,  78, ...,  66,  96, 130], dtype=uint8), 'image_name': array(b'kodim18.png', dtype='|S11')}
example 23: {'image': array([137,  80,  78, ...,  66,  96, 130], dtype=uint8), 'image_name': array(b'kodim19.png', dtype='|S11')}
>> total rows: 24
create successfully

```

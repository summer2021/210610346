"""write dtd data to mindrecord file"""
import os
import numpy as np
from mindspore.mindrecord import FileWriter


DTD_IMAGE_PATH = "dataset/dtd/images"
DTD_LABELS_IMAGE = "dataset/dtd/labels"


#label to number
label_set=dict()
num=0

for root, dirs, files in os.walk(DTD_IMAGE_PATH):
    #get label name from folder
    for i in dirs:
        label_set[i]=num
        num+=1
        
def get_data_as_dict_train(i):
    """get train data from dataset"""
    train=open(os.path.join(DTD_LABELS_IMAGE,"train"+str(i)+".txt"))
    for line in train:
        # train.txt, get label and filename
        data={}
        label_and_imagename = line.split("/") #banded/banded_0011.jpg
        image_label=label_and_imagename[0] #banded
        image_name=label_and_imagename[1] #banded_0011.jpg
        image_path=os.path.join(os.path.join(DTD_IMAGE_PATH,line.replace("\n", "")))
        image_content=open(image_path, "rb")
        image_bytes = image_content.read()
        image_content.close()
        
        data["image"] = image_bytes
        data["image_name"] = image_name 
        data["label"]=label_set[image_label]#0-46
        data["label_name"]=image_label
        
        yield data
    train.close()
    
def get_data_as_dict_test(i):
    """get test data from dataset"""
    # test.txt, get label and filename
    test=open(os.path.join(DTD_LABELS_IMAGE,"test"+str(i)+".txt"))
    for line in test:
        # images.txt, get id and filename
        data={}
        label_and_imagename = line.split("/")
        image_label=label_and_imagename[0]
        image_name=label_and_imagename[1]
        image_path=os.path.join(os.path.join(DTD_IMAGE_PATH,line.replace("\n", "")))
        image_content=open(image_path, "rb")
        image_bytes = image_content.read()
        image_content.close()
        
        data["image"] = image_bytes 
        data["image_name"] = image_name         
        data["label"]=label_set[image_label]       
        data["label_name"]=image_label
        
        yield data
    test.close()
    
def get_data_as_dict_val(i):
    """get val data from dataset"""
    # val.txt, get label and filename
    val=open(os.path.join(DTD_LABELS_IMAGE,"val"+str(i)+".txt"))  
    
    for line in val:
        data={}
        label_and_imagename = line.split("/")
        image_label=label_and_imagename[0]
        image_name=label_and_imagename[1]
        image_path=os.path.join(os.path.join(DTD_IMAGE_PATH,line.replace("\n", "")))                
        image_content=open(image_path, "rb")
        image_bytes = image_content.read()
        image_content.close()
        
        data["image"] = image_bytes                
        data["image_name"] = image_name        
        data["label"]=label_set[image_label]      
        data["label_name"]=image_label
        
        yield data
    val.close()
    
def gen_mindrecord():
    # train test val 1.txt-10.txt
    for i in range(1,11):
        fw_train = FileWriter(file_name="output/train"+str(i)+".mindrecord")
        fw_test =FileWriter(file_name="output/test"+str(i)+".mindrecord")
        fw_val =FileWriter(file_name="output/val"+str(i)+".mindrecord")
        
        schema = {"image": {"type": "bytes"},
                  "image_name": {"type": "string"},
                  "label": {"type": "int32"},
                  "label_name": {"type": "string"}
                 }
        
        fw_train.add_schema(schema, "dtd dataset")
        fw_test.add_schema(schema, "dtd dataset")
        fw_val.add_schema(schema, "dtd dataset")
        
        get_data_iter_train= get_data_as_dict_train(i)
        get_data_iter_test=get_data_as_dict_test(i)
        get_data_iter_val=get_data_as_dict_val(i)
        
        batch_size = 256
        transform_count_train = 0
        transform_count_test = 0
        transform_count_val = 0
        
        while True:
            data_list_train = []
            data_list_test=[]
            data_list_val=[]
            try:
                for _ in range(batch_size):
                    data_list_train.append(get_data_iter_train.__next__())
                    data_list_test.append(get_data_iter_test.__next__())
                    data_list_val.append(get_data_iter_val.__next__())
                    transform_count_train += 1
                    transform_count_test += 1
                    transform_count_val += 1
                fw_train.write_raw_data(data_list_train)
                fw_test.write_raw_data(data_list_test)
                fw_val.write_raw_data(data_list_val)
                print(">> transformed {} train{} record...".format(transform_count_train,i))
                print(">> transformed {} test{} record...".format(transform_count_test,i))
                print(">> transformed {} val{} record...".format(transform_count_val,i))
            except StopIteration:
                if data_list_train:
                    fw_train.write_raw_data(data_list_train)
                
                if data_list_test:
                    fw_test.write_raw_data(data_list_test)
                
                if data_list_val:
                    fw_val.write_raw_data(data_list_val)
                  
                print(">> transformed {} train{} record...".format(transform_count_train,i))
                print(">> transformed {} test{} record...".format(transform_count_test,i))
                print(">> transformed {} val{} record...".format(transform_count_val,i))
                break
    
        fw_train.commit()
        fw_test.commit()
        fw_val.commit()
    print(">>generated successfully")


def main():
    # generate mindrecord
    print(">> begin generate mindrecord")
    gen_mindrecord()

if __name__ == "__main__":
    main()

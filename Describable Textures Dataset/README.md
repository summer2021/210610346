# Guideline to Transfer Describable Textures Dataset to MindRecord

<!-- TOC -->

- [What does the example do](#what-does-the-example-do)
- [How to use the example to generate MindRecord](#how-to-use-the-example-to-generate-mindrecord)
    - [Download Download Describable Textures Dataset and unzip and unzip](#download-describable-textures-dataset-and-unzip)
    - [Generate MindRecord](#generate-mindrecord)
    - [Create MindDataset By MindRecord](#create-minddataset-by-mindrecord)

<!-- /TOC -->

## What does the example do

This example is used to read data from Describable Textures Dataset and generate mindrecord. It just transfers the Describable Textures Dataset to mindrecord without any data preprocessing. You can modify the example or follow the example to implement your own example.

1. run.sh: generate MindRecord entry script.
    - gen_mindrecord.py : read the Describable Textures Dataset and transfer it to mindrecord.
2. run_read.py: create MindDataset by MindRecord entry script.
    - create_dataset.py: use MindDataset to read MindRecord to generate dataset.

## How to use the example to generate MindRecord

Download Describable Textures Dataset, transfer it to mindrecord, use MindDataset to read mindrecord.

### Download Describable Textures Dataset and unzip

1. Download the training data zip.
    > [Describable Textures Dataset download address](https://www.robots.ox.ac.uk/~vgg/data/dtd/)  
    > **-> Download -> dtd-r1.0.1.tar.gz**   

2. Unzip the data to dir example/cv_to_mindrecord/Describable Textures Dataset/dataset.

```bash
    tar -zxvf dtd-r1.0.1.tar.gz -C {your-mindspore}/example/cv_to_mindrecord/Describable Textures Dataset/dataset/
```

 The unzip should like this:

```bash
$ ls {your-mindspore}/example/cv_to_mindrecord/Describable Textures Dataset/dataset/
dtd
```

### Generate MindRecord

1.Run the run.sh script.

```bash
bash run.sh
```

2.Output like this:

```bash

>> begin generate mindrecord
>> transformed 256 train1 record...
>> transformed 256 test1 record...
>> transformed 256 val1 record...
>> transformed 512 train1 record...
>> transformed 512 test1 record...
>> transformed 512 val1 record...
>> transformed 768 train1 record...
>> transformed 768 test1 record...
>> transformed 768 val1 record...
>> transformed 1024 train1 record...
>> transformed 1024 test1 record...
>> transformed 1024 val1 record...
>> transformed 1280 train1 record...
>> transformed 1280 test1 record...
>> transformed 1280 val1 record...
>> transformed 1536 train1 record...
>> transformed 1536 test1 record...
>> transformed 1536 val1 record...
>> transformed 1792 train1 record...
>> transformed 1792 test1 record...
>> transformed 1792 val1 record...
>> transformed 1880 train1 record...
>> transformed 1880 test1 record...
>> transformed 1880 val1 record...
>> transformed 256 train2 record...
>> transformed 256 test2 record...
>> transformed 256 val2 record...
>> transformed 512 train2 record...
>> transformed 512 test2 record...
>> transformed 512 val2 record...
>> transformed 768 train2 record...
>> transformed 768 test2 record...
>> transformed 768 val2 record...
...
>>generated successfully

```

3.Generate mindrecord files

```bash
$ ls output/
train1.mindrecord  train1.mindrecord.db test1.mindrecord  test1.mindrecord.db
```

### Create MindDataset By MindRecord

1. Run the run_read.sh script.

```bash
bash run_create.sh
 ```

2. Output like this:

```bash
   example 100: {'image': array([255, 216, 255, ..., 163, 255, 217], dtype=uint8), 'image_name': array(b'potholed_0161.jpg\n', dtype='|S18'), 'label': array(32, dtype=int32), 'label_name': array(b'potholed', dtype='|S8')}
example 200: {'image': array([255, 216, 255, ...,  63, 255, 217], dtype=uint8), 'image_name': array(b'swirly_0169.jpg\n', dtype='|S16'), 'label': array(41, dtype=int32), 'label_name': array(b'swirly', dtype='|S6')}
example 300: {'image': array([255, 216, 255, ..., 159, 255, 217], dtype=uint8), 'image_name': array(b'freckled_0109.jpg\n', dtype='|S18'), 'label': array(13, dtype=int32), 'label_name': array(b'freckled', dtype='|S8')}
example 400: {'image': array([255, 216, 255, ...,  15, 255, 217], dtype=uint8), 'image_name': array(b'pleated_0146.jpg\n', dtype='|S17'), 'label': array(29, dtype=int32), 'label_name': array(b'pleated', dtype='|S7')}
example 500: {'image': array([255, 216, 255, ...,  31, 255, 217], dtype=uint8), 'image_name': array(b'knitted_0122.jpg\n', dtype='|S17'), 'label': array(20, dtype=int32), 'label_name': array(b'knitted', dtype='|S7')}
example 600: {'image': array([255, 216, 255, ..., 179, 255, 217], dtype=uint8), 'image_name': array(b'veined_0177.jpg\n', dtype='|S16'), 'label': array(42, dtype=int32), 'label_name': array(b'veined', dtype='|S6')}
example 700: {'image': array([255, 216, 255, ..., 175, 255, 217], dtype=uint8), 'image_name': array(b'perforated_0035.jpg\n', dtype='|S20'), 'label': array(27, dtype=int32), 'label_name': array(b'perforated', dtype='|S10')}
example 800: {'image': array([255, 216, 255, ...,  95, 255, 217], dtype=uint8), 'image_name': array(b'crosshatched_0079.jpg\n', dtype='|S22'), 'label': array(8, dtype=int32), 'label_name': array(b'crosshatched', dtype='|S12')}
example 900: {'image': array([255, 216, 255, ..., 191, 255, 217], dtype=uint8), 'image_name': array(b'woven_0039.jpg\n', dtype='|S15'), 'label': array(44, dtype=int32), 'label_name': array(b'woven', dtype='|S5')}
example 1000: {'image': array([255, 216, 255, ...,  31, 255, 217], dtype=uint8), 'image_name': array(b'smeared_0140.jpg\n', dtype='|S17'), 'label': array(34, dtype=int32), 'label_name': array(b'smeared', dtype='|S7')}
example 1100: {'image': array([255, 216, 255, ...,   8, 255, 217], dtype=uint8), 'image_name': array(b'fibrous_0162.jpg\n', dtype='|S17'), 'label': array(11, dtype=int32), 'label_name': array(b'fibrous', dtype='|S7')}
example 1200: {'image': array([255, 216, 255, ..., 127, 255, 217], dtype=uint8), 'image_name': array(b'frilly_0012.jpg\n', dtype='|S16'), 'label': array(14, dtype=int32), 'label_name': array(b'frilly', dtype='|S6')}
example 1300: {'image': array([255, 216, 255, ..., 175, 255, 217], dtype=uint8), 'image_name': array(b'lacelike_0066.jpg\n', dtype='|S18'), 'label': array(21, dtype=int32), 'label_name': array(b'lacelike', dtype='|S8')}
example 1400: {'image': array([255, 216, 255, ..., 127, 255, 217], dtype=uint8), 'image_name': array(b'meshed_0161.jpg\n', dtype='|S16'), 'label': array(25, dtype=int32), 'label_name': array(b'meshed', dtype='|S6')}
example 1500: {'image': array([255, 216, 255, ..., 183, 255, 217], dtype=uint8), 'image_name': array(b'stratified_0069.jpg\n', dtype='|S20'), 'label': array(38, dtype=int32), 'label_name': array(b'stratified', dtype='|S10')}
example 1600: {'image': array([255, 216, 255, ...,  63, 255, 217], dtype=uint8), 'image_name': array(b'lacelike_0051.jpg\n', dtype='|S18'), 'label': array(21, dtype=int32), 'label_name': array(b'lacelike', dtype='|S8')}
example 1700: {'image': array([255, 216, 255, ..., 127, 255, 217], dtype=uint8), 'image_name': array(b'veined_0094.jpg\n', dtype='|S16'), 'label': array(42, dtype=int32), 'label_name': array(b'veined', dtype='|S6')}
example 1800: {'image': array([255, 216, 255, ...,  63, 255, 217], dtype=uint8), 'image_name': array(b'swirly_0082.jpg\n', dtype='|S16'), 'label': array(41, dtype=int32), 'label_name': array(b'swirly', dtype='|S6')}
>> read data1 rows: 1880
>> total data1 rows: 1880
>>create successfully
```


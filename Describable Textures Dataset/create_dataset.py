"""create Describable Textures Datasetdata to MindDataset """
import mindspore.dataset as ds

def create_dataset(data_file,i):
    """create MindDataset"""
    num_readers = 4
    data_set = ds.MindDataset(dataset_file=data_file,
                              num_parallel_workers=num_readers,
                              shuffle=True)
    index = 0
    for item in data_set.create_dict_iterator(output_numpy=True, num_epochs=1):
        index += 1
        if index % 100==0:
            print("example {}: {}".format(index, item))
        
        if index % 1880 == 0:
            print(">> read data{} rows: {}".format(i,index))
    print(">> total data{} rows: {}".format(i,index))

if __name__ == '__main__':
    for i in range(1,11):
        create_dataset('output/train'+str(i)+'.mindrecord',i)
        create_dataset('output/test'+str(i)+'.mindrecord',i)
        create_dataset('output/val'+str(i)+'.mindrecord',i)
    print(">>create successfully")